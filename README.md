# ProjetSystemeDocker

## Lancement des différents containers
Pour lancer les containers git et jenkins il faut lancer le docker-compose associé au dossier docker_compose_git_jenkins

```
docker-compose up -d
```

Il faut ensuite se rendre sur 127.0.0.1:8080 pour effectuer la configuration de Jenkins puis sur 127.0.0.1:3000 pour la confguration de Git.

## Rapport du ProjetSystemeDocker
https://gitlab.com/ludovic.witz/projetsystemedocker/-/blob/master/Rapport_Docker_Hebinger_Witz.pdf

## Crédits
Hebinger Pierre et Witz Ludovic
